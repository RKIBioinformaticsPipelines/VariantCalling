#!/usr/bin/env python3
## @file 
#  Main file to call the pipeline.
import argparse
import os
import re
import sys
import subprocess
import yaml
import modules
## Current version.
__version__ = '3.2.0'
# https://stackoverflow.com/questions/20094215
## Class for a custom help function for subparsers.
class _HelpAction(argparse._HelpAction):

    ## Pint help for each subparser.
    #  @param self The object pointer.
    #  @param parser The parser object to print help for.
    #  @param namespace The used namespace.
    #  @param values The used values.
    #  @param option_string Optional option string.
    def __call__(self, parser, namespace, values, option_string=None):
        parser.print_help()

        subparsers_actions = [
            action for action in parser._actions
            if isinstance(action, argparse._SubParsersAction)]
        for subparsers_action in subparsers_actions:
            for choice, subparser in subparsers_action.choices.items():
                print("\nSubcommand '{}'".format(choice))
                print(subparser.format_help())

        parser.exit()

# https://bitbucket.org/ruamel/std.argparse
## Sets a default subparser for a parser.
#
#  @param self The object pointer.
#  @param name Name of the subparser to use.
#  @param args Arguments that the parser was called with.
def set_default_subparser(self, name, args=None):
    subparser_found = False
    for arg in sys.argv[1:]:
        if arg in ('-h', '--help'):
            break
    else:
        for x in self._subparsers._actions:
            if not isinstance(x, argparse._SubParsersAction):
                continue
            for sp_name in x._name_parser_map.keys():
                if sp_name in sys.argv[1:]:
                    subparser_found = True
        if not subparser_found:
            if args is None:
                sys.argv.insert(1, name)
            else:
                args.insert(0, name)

## Checks whether there is a GATK executable in a directory.
#  @param f Directory to check.
def _valid_gatk(f):
    if os.path.isfile(os.path.join(os.path.realpath(f), 'GenomeAnalysisTK.jar')):
        return f
    else:
        raise argparse.ArgumentTypeError('Cannot find GenomeAnalysisTK.jar in folder {}'.format(os.path.realpath(f)))

## Checks whether there is a Picard executable in a directory.
#  @param f Directory to check.
def _valid_picard(f):
    if os.path.isfile(os.path.join(os.path.realpath(f), 'picard.jar')):
        return f
    else:
        raise argparse.ArgumentTypeError('Cannot find picard.jar in folder {}'.format(os.path.realpath(f)))

## Checks whether there is a ElPrep executable in a directory.
#  @param f Directory to check.
def _valid_elprep(f):
    if os.path.isfile(os.path.join(os.path.realpath(f), 'elprep')):
        return f
    else:
        raise argparse.ArgumentTypeError('Cannot find elprep in folder {}'.format(os.path.realpath(f)))

## Checks whether there is a SnpEff executable in a directory.
#  @param f Directory to check.
def _valid_snpeff(f):
    if os.path.isfile(os.path.join(os.path.realpath(f), 'snpEff.jar')):
        return f
    else:
        raise argparse.ArgumentTypeError('Cannot find snpEff.jar in folder {}'.format(os.path.realpath(f)))

## Checks whether there is a samtools executable in a directory.
#  @param f Directory to check.
def _valid_samtools(f):
    if os.path.isfile(os.path.join(os.path.realpath(f), 'samtools')):
        return f
    else:
        raise argparse.ArgumentTypeError('Cannot find samtools in folder {}'.format(os.path.realpath(f)))

## Checks whether the Java version is => 1.8.
#  @param f Java executable to check.
def _valid_java(f):
    vn_re = re.compile("version.+?([\d\.]+)")
    try:
        version_string = subprocess.check_output(f + " -version", shell=True, stderr=subprocess.STDOUT)
    except:
        raise argparse.ArgumentTypeError('It appears that {} is not a java executable (absolute or available in the PATH environment)'.format(f))
    try:
        version = vn_re.search(str(version_string))
        vn = version.group(1)
        vn = float(".".join(vn.split(".")[:2]))
    except Exception as e:
        print(e)
        raise argparse.ArgumentTypeError('It appears that {} is not a java executable (absolute or available in the PATH environment)'.format(f))
    if vn < 1.8:
        raise argparse.ArgumentTypeError('{} is java version {}, however, at least version 1.8 is required'.format(f, vn))
    return f

## Main call.
def main():
    # Parse any conf_file specification
    # We make this parser with add_help=False so that
    # it doesn't parse -h and print help.
    conf_parser = argparse.ArgumentParser(
        description=__doc__, # printed with -h/--help
        # Don't mess with format of description
        formatter_class=argparse.RawDescriptionHelpFormatter,
        # Turn off help, so we print all options in response to -h
        add_help=False
    )
    ycd = dict()
    ycd['reference'] = ['--reference', '-r']
    ycd['input'] = ['--input', '-i']
    ycd['picard'] = ['--picard']
    ycd['snpeff'] = ['--snpeff']
    ycd['gatk'] = ['--gatk']
    ycd['samtools'] = ['--samtools']
    ycd['elprep'] = ['--elprep']
    ycd['output'] = ['--output', '-o']
    ycd['log'] = ['--log', '-l']
    ycd['annotate_all'] = ['--annotate_all']
    ycd['decimal_sep'] = ['--decimal_sep']
    ycd['threads'] = ['--threads', '-t']
    ycd['platform'] = ['--platform', '-p']
    ycd['create_config'] = ['--create_config']
    ycd['ploidy'] = ['--ploidy']
    ycd['msa'] = ['--msa']
    ycd['ambiguity_cutoff'] = ['--ambiguity_cutoff']
    ycd['majority'] = ['--majority']
    ycd['java'] = ['--java']
    ycd['snpfilter'] = ['--snpfilter']
    ycd['snpfilter_config'] = ['--snpfilter_config']
    ycd['config'] = ['--config', '-c']
    def _check(ycd, key, argv):
        for entry in ycd[key]:
            if entry in argv:
                return False
        return True
    config = os.path.join(os.path.dirname(__file__), 'run.yaml')
    conf_parser.add_argument('-c', '--config', help="Specify config file", metavar="FILE")
    conf_args, remaining_argv = conf_parser.parse_known_args()
    if conf_args.config:
        remaining_argv += ['--config', conf_args.config]
        d1 = yaml.safe_load(open(conf_args.config, 'r'))
        if os.path.isfile(config):
            d2 = yaml.safe_load(open(config, 'r'))
            for key, value in d2.items():
                d1[key] = value
        for key, value in d1.items():
            if _check(ycd, key, sys.argv):
                if type(value) != bool and value is not None:
                    remaining_argv += [ycd[key][0], str(value)]
                elif value:
                    remaining_argv += [ycd[key][0]]
    if os.path.isfile(config):
        d = yaml.safe_load(open(config, 'r'))
        for key, value in d.items():
            if _check(ycd, key, sys.argv):
                if type(value) != bool and value is not None:
                    remaining_argv += [ycd[key][0], str(value)]
                elif value:
                    remaining_argv += [ycd[key][0]]

    parser = argparse.ArgumentParser(add_help = False, prog='Variant Calling Pipeline')
    parser.add_argument('-h', '--help', action = _HelpAction, help ="show this help message and exit")
    parser.add_argument('--version', '-v', action='version', version='%(prog)s ' + __version__)
    required = parser.add_argument_group("required arguments")
    optional = parser.add_argument_group("optional arguments")
    required.add_argument("--reference", "-r", type = str, help = "Path to GenBank file")
    required.add_argument("--input", "-i", type = str, help = "Mapping file in either SAM or BAM format")
    required.add_argument("--picard", required = True, type = _valid_picard, help = "Path to picard directory")
    required.add_argument("--snpeff", required = True, type = _valid_snpeff, help = "Path to snpEff directory")
    required.add_argument("--gatk", required = True, type = _valid_gatk, help = "Path to GATK directory")
    optional.add_argument("--samtools", type = _valid_samtools, default = '/usr/bin/', help = "Path to samtools directory. (Default: /usr/bin/)")
    optional.add_argument("--elprep", type = _valid_elprep, default = None, help = "Path to elPrep directory")
    optional.add_argument("--output", "-o", type = str, default = '.', help = "Output directory")
    optional.add_argument("--log", "-l", type = str, default = None, help = "Where the log shall be saved")
    optional.add_argument("--annotate_all", action = "store_true", default = False, help = "Also report variants in non-coding regions")
    optional.add_argument("--decimal_sep", type = str, default = ",", help = "Set the default decimal separator, e.g. 10.323 or 10,323. (Default: %(default)s)")
    optional.add_argument("--threads", "-t", type = int, default = 1, help = "Set maximal number of threads")
    optional.add_argument("--platform", "-p", dest='platform', choices=['ILLUMINA', 'SOLID', 'LS454', 'HELICOS', 'PACBIO'], default='ILLUMINA', help='Choose sequencer platform. (default: ILLUMINA)')
    #optional.add_argument("--create_config", type = str, default = None, nargs = "?", help = "Create configfile for re-use in given location")
    optional.add_argument("--ploidy", type = str, default = "1", nargs = "?", help = "Sample ploidy for GATK. Default: 1")
    optional.add_argument("--msa", action = "store_true", default = False, help = "Perform multiple sequence alignment of consensus sequences")
    optional.add_argument("--ambiguity_cutoff", type = int, default = 5, nargs = "?", help = "Maximal difference in coverage for called alleles to be ambiguous encoded in msa.")
    optional.add_argument("--majority", action = "store_true", default = False, help = "Use majority vote for called alleles.")
    optional.add_argument("--snpfilter", action = "store_true", default = False, help = "Run snpfilter after MSA. Enforces MSA.")
    optional.add_argument("--snpfilter_config", type = str, default = '', help = "Options for snpfilter. Usage: --snpfilter_config=\"<options>\"")
    optional.add_argument('--java', dest='java', nargs='?', default='java', type=_valid_java, help='Java version 1.8+ executable.')
    optional.add_argument("--config", "-c", type = str, help = "Configfile in json format")
    (args, uargs) = parser.parse_known_args(remaining_argv)
    os.makedirs(args.output, exist_ok=True)
    if args.elprep is None:
        args.elprep = 'NA' # snakemake doesn't allow config to be set to None for a value anymore
    if args.snpfilter:
        args.msa = True
    if args.log is None:
        args.log = os.path.join(args.output, 'log.txt')
    d = {"reference" : "GB_FILE", "input" : "SAM_FILE", "snpeff" : "SNPEFF_FOLDER", "picard" : "PICARD_FOLDER", "elprep" : "ELPREP_FOLDER", "log" : "LOG", "annotate_all" : "ANNOTATE_ALL", "decimal_sep" : "DECIMAL", "threads" : "THREADS", "gatk" : "GATK_FOLDER", "output" : "OUTPUT", "ploidy" : "PLOIDY", "msa" : "MSA", "ambiguity_cutoff" : "AMBIGUITY_CUTOFF", "majority" : "MAJORITY", "java" : "JAVA", "snpfilter" : "SNPFILTER", "snpfilter_config" : "SNPFILTER_CONFIG", "platform" : "PLATFORM"}
    with open(args.log, 'w') as f:
        f.write("{}\n".format(' '.join(sys.argv)))
        f.write("snakemake --snakefile {} --config {}={} {}={} {}={} {}={} {}={} {}={} {}={} {}={} {}={} {}={} {}={} {}={} {}={} {}={} {}={} {}={} {}={} {}={} {}={} --cores {} {}\n".format(os.path.realpath(os.path.join(modules.__path__[0], 'Snakefile')), 'GB_FILE', args.reference, 'SAM_FILE', args.input, 'ELPREP_FOLDER', args.elprep, 'GATK_FOLDER', args.gatk, 'SNPEFF_FOLDER', args.snpeff, 'PICARD_FOLDER', args.picard, 'LOG', args.log, 'ANNOTATE_ALL', args.annotate_all, 'DECIMAL', args.decimal_sep, 'PLOIDY', args.ploidy, 'MSA', args.msa, 'AMBIGUITY_CUTOFF', args.ambiguity_cutoff, 'MAJORITY', args.majority, 'OUTPUT', args.output, 'JAVA', args.java, 'SNPFILTER', args.snpfilter, 'SNPFILTER_CONFIG', args.snpfilter_config, 'SAMTOOLS_FOLDER', args.samtools, 'PLATFORM', args.platform, args.threads, ' '.join(uargs)))
    subprocess.call("snakemake --snakefile {} --config {}={} {}={} {}={} {}={} {}={} {}={} {}={} {}={} {}={} {}={} {}={} {}={} {}={} {}={} {}={} {}={} {}={} {}={} {}={} --cores {} {}".format(os.path.realpath(os.path.join(modules.__path__[0], 'Snakefile')), 'GB_FILE', args.reference, 'SAM_FILE', args.input, 'ELPREP_FOLDER', args.elprep, 'GATK_FOLDER', args.gatk, 'SNPEFF_FOLDER', args.snpeff, 'PICARD_FOLDER', args.picard, 'LOG', args.log, 'ANNOTATE_ALL', args.annotate_all, 'DECIMAL', args.decimal_sep, 'PLOIDY', args.ploidy, 'MSA', args.msa, 'AMBIGUITY_CUTOFF', args.ambiguity_cutoff, 'MAJORITY', args.majority, 'OUTPUT', args.output, 'JAVA', args.java, 'SNPFILTER', args.snpfilter, 'SNPFILTER_CONFIG', args.snpfilter_config, 'SAMTOOLS_FOLDER', args.samtools, 'PLATFORM', args.platform, args.threads, ' '.join(uargs)), shell = True)
    if False and args.create_config is not None:
        with open(args.create_config, 'w') as f:
            f.write("{\n")
            v = vars(args)
            v.pop("create_config")
            l = len(v)
            for i, arg in enumerate(v):
                try:
                    if i < l - 1:
                        f.write('{} : {},\n'.format(d[arg], getattr(args, arg)))
                    else:
                        f.write('{} : {}\n'.format(d[arg], getattr(args, arg)))
                except KeyError:
                    continue
            f.write('}\n')

if __name__ == "__main__":
    main()
